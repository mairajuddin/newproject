<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;

class EmployeesTable extends Controller
{
    public function addEmployees(Request $request){
        $emp=new Employees();
        $emp->first_name=$request['first_name'];
        $emp->last_name=$request['last_name'];
        $emp->email=$request['email'];
        $emp->salary=$request['salary'];
        $emp->join_date=$request['join_date'];
        $emp->save();
        return response()->json(['success'=>true,'message'=>'Employees Added']);
    }
}
