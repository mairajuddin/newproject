<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class SuperAdminController extends Controller
{
    //
    public function addUser(Request $request){
        $user=new User();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->password=$request['password'];
        $user->role_id=2;
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Added']);
    }
    public function editUser(Request $request)
    {
        $user = User::find($request['id']);
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->password=$request['password'];
        $user->role_id=2;
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Edited']);
    }
    public function deleteUser(Request $request)
    {
        $user = User::find($request['id']);
        $user->delete();
      //  $user->save();
        return response()->json(['success'=>true,'message'=>'User Deleted']);
    }

    public function selectUser()
    {
        $user = User::get();
       // $user->delete();
        //  $user->save();
        return response()->json(['success'=>true,'users'=>$user]);
    }
}
